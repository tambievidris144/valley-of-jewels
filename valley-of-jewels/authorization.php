<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./front/style.css">
    <title>Вход</title>
</head>
<body style="background-image: url(./front/assets/images/background/ворота.jpg); background-size: cover;">

    <div style="display: flex; flex-direction: row; margin-top: 30%; margin-left: 10%;">
        <div id="registration" class="text_box">
            <form action="./authorization.php" method="post" >
                <span class="menu_reg_text">Имя</span><br><input type="text" name="name" autocomplete="off">
                <span class="menu_reg_text">Фамилия</span> <input type="text" name="surname" autocomplete="off">
                <span class="menu_reg_text">Email</span> <input type="email" id="email" name="email_user" autocomplete="off">
                <span class="menu_reg_text">Придумайте логин</span> <input type="text" name="login" autocomplete="off">
                <span class="menu_reg_text">Придумайте пароль</span> <input type="password" name="password" autocomplete="off">
                <span class="menu_reg_text">Повторите пароль</span> <input type="password" name="password_again" autocomplete="off">
                <br>
                <br>
                <input type="submit" name="button" value="Регистрация"></input>
            </form>
        </div>
        <div id="authorization" class="text_box">
        <form action="./authorization.php" method="post">
            <span class="menu_reg_text">Логин</span> <input type="text" name="login2" autocomplete="off">
            <span class="menu_reg_text">Пароль</span> <input type="password" name="password2" autocomplete="off">
            <br>
            <br>
            <input type="submit" name="button" value="Вход"></input>
        </form>
        </div>
    </div>

    <?php 
    if(isset($_POST['button'])){
    $button=trim(htmlspecialchars($_POST['button']));
    $servername = 'localhost';
    $username = "root";
    $local_password = "";
    $dbname = "test";
    if($button=="Вход"){
        require "./log_in.php";
    }elseif($button=="Регистрация"){
        $name=stripslashes($_POST['name']);
        $surname=stripslashes($_POST['surname']);
        $login=trim(htmlspecialchars($_POST['login']));
        $password=trim(htmlspecialchars($_POST['password']));
        $password2=trim(htmlspecialchars($_POST['password_again']));
        $email = $_POST['email_user'] ?? '';
        require "./registration.php";
}
    }
?> 
</body>
</html>