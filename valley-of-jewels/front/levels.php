<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="levels_style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <?php
    if (isset($_GET['id'])){
       $id=($_GET['id']); 
       $level=($_GET['level']);
    }
    $servername = 'localhost';
       $username = "root";
        $local_password = "";
        $dbname = "test";
         $conn = new mysqli($servername, $username, $local_password, $dbname);
        if ($conn->connect_error) {
           die("Connection failed: " . $conn->connect_error);
        } 
        $sql = "SELECT name,surname FROM users WHERE `id`='$id'";
        $result=mysqli_query($conn, $sql);
        if ($result) {
        $row = mysqli_fetch_row($result);
        //echo '<div class="record_points">Ваш рекорд: ' , $row[0] ,'</div>';
        } else {
       echo "Error: " . mysqli_error($conn);
        }
        $conn->close();
        mysqli_free_result($result);
    ?>

    <div class="user" style="width:21%;">
        <span><?php echo $row[0],"  ",$row[1];?></span>
        <a href="./user_statistic.php?id=<?php echo $id?>" target="_blank" style="margin-left:1%;">Статистика</a>
        <a class="button_window" href="../index.php" style="margin-left:1%;">Выход</a>
          </div>
    
    <div class="big_levels_div">
        <div id="chapter_1" class="levels">
            <label class="chapters_label">Глава 1</label>
            <form action="./chapter_1.php?id=<?php echo $id;?>" method="post">
                <input id="1" class="level_buttons block" type="submit" name="level_name" value="Уровень 1">
            </form>
            <form action="./chapter_1.php?id=<?php echo $id;?>" method="post" >
                <input id="2" class="level_buttons block" type="submit" name="level_name" value="Уровень 2">
            </form>
            <form action="./chapter_1.php?id=<?php echo $id;?>" method="post">
                <input id="3" class="level_buttons block" type="submit" name="level_name" value="Уровень 3">
            </form>
            <form action="./chapter_1.php?id=<?php echo $id;?>" method="post">
                <input id="4" class="level_buttons block" type="submit" name="level_name" value="Уровень 4">
            </form>
        </div>
        <div id="chapter_2" class="levels">
            <label class="chapters_label">Глава 2</label>
            <form action="./chapter_2.php?id=<?php echo $id;?>" method="post">
                <input id="5" class="level_buttons block" type="submit" name="level_name" value="Уровень 1">
            </form>
            <form action="./chapter_2.php?id=<?php echo $id;?>" method="post" >
                <input id="6" class="level_buttons block" type="submit" name="level_name" value="Уровень 2">
            </form>
            <form action="./chapter_2.php?id=<?php echo $id;?>" method="post">
                <input id="7" class="level_buttons block" type="submit" name="level_name" value="Уровень 3">
            </form>
            <form action="./chapter_2.php?id=<?php echo $id;?>" method="post">
                <input id="8" class="level_buttons block" type="submit" name="level_name" value="Уровень 4">
            </form>
        </div>

        <div id="chapter_3" class="levels">
            <label class="chapters_label">Глава 3</label>
            <form action="./chapter_3.php?id=<?php echo $id;?>" method="post">
                <input id="9" class="level_buttons block" type="submit" name="level_name" value="Уровень 1">
            </form>
            <form action="./chapter_3.php?id=<?php echo $id;?>" method="post" >
                <input id="10" class="level_buttons block" type="submit" name="level_name" value="Уровень 2">
            </form>
            <form action="./chapter_3.php?id=<?php echo $id;?>" method="post">
                <input id="11" class="level_buttons block" type="submit" name="level_name" value="Уровень 3">
            </form>
            <form action="./chapter_3.php?id=<?php echo $id;?>" method="post">
                <input id="12" class="level_buttons block" type="submit" name="level_name" value="Уровень 4">
            </form>
        </div>
        <div id="chapter_4" class="levels">
            <label class="chapters_label">Глава 4</label>
            <form action="./chapter_4.php?id=<?php echo $id;?>" method="post">
                <input id="13" class="level_buttons block" type="submit" name="level_name" value="Уровень 1">
            </form>
            <form action="./chapter_4.php?id=<?php echo $id;?>" method="post" >
                <input id="14" class="level_buttons block" type="submit" name="level_name" value="Уровень 2">
            </form>
            <form action="./chapter_4.php?id=<?php echo $id;?>" method="post">
                <input id="15" class="level_buttons block" type="submit" name="level_name" value="Уровень 3">
            </form>
            <form action="./chapter_4.php?id=<?php echo $id;?>" method="post">
                <input id="16" class="level_buttons block" type="submit" name="level_name" value="Уровень 4">
            </form>
        </div>

    </div>
    <form action="./extreme.php?id=<?php echo $id;?>" method="post">
        <button id="17" class="bonus_level_button block" type="submit">Экстрим</button>
    </form>
    <script src="./block.js"></script>
</body>
</html>