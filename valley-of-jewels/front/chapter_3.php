<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>3 в ряд</title>
    <link rel="stylesheet" href="./levels/chapter_3/style.css">
    <script src="./assets/js/phaser.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="steps">

    </div>

    <div class="rules">
     
    </div>
    <div id="game_over" class="invisible">
        <div id="child-game_over">

        </div>
        
    </div>
    <div id="game_complete" class="invisible">
        <div id="child-game_complete">

        </div>
        
    </div>

    <?php
    if (isset($_GET['id'])) {
        $id=($_GET['id']);
     }
    if (isset($_POST)) $level_name=htmlspecialchars($_POST['level_name']);
    ?>
    <?php if ($level_name == "Уровень 1"): ?>
        <div class="game_field">
            <script src="./levels/chapter_3/level_1.js"></script>
        </div>
    <?php elseif($level_name == "Уровень 2"): ?>
            <div class="game_field">
            <script src="./levels/chapter_3/level_2.js"></script>
        </div>
    <?php elseif ($level_name == "Уровень 3"): ?>
            <div class="game_field">
            <script src="./levels/chapter_3/level_3.js"></script>
        </div>
    <?php elseif ($level_name == "Уровень 4"): ?>
            <div class="game_field">
            <script src="./levels/chapter_3/level_4.js"></script>
        </div>
    <?php endif; ?>
</body>
</html>