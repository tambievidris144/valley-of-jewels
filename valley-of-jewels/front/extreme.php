<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>3 в ряд</title>
    <link rel="stylesheet" href="./levels/extreme/style.css">
    <script src="./assets/js/phaser.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="timer">
    </div>
    <div class="rules">
    </div>
    <div id="game_over" class="invisible">
        <div id="child-game_over">
        </div>
    </div>
    <div class="game_field">
            <script src="./levels/extreme/extreme_level.js"></script>
    </div>
    <div class="record">
    <?php
    if (isset($_GET['id'])) {
        $id=($_GET['id']);
     }
     $servername = 'localhost';
     $username = "root";
     $local_password = "";
     $dbname = "test";
    $conn = new mysqli($servername, $username, $local_password, $dbname);
    if ($conn->connect_error) {
       die("Connection failed: " . $conn->connect_error);
    } 
    $sql = "SELECT record FROM extreme_record WHERE `id_user`='$id'";
    $result=mysqli_query($conn, $sql);
    if ($result) {
    $row = mysqli_fetch_row($result);
    echo '<div class="record_points">Ваш рекорд: ' , $row[0] ,'</div>';
    } else {
       echo "Error: " . mysqli_error($conn);
    }
    $conn->close();
    mysqli_free_result($result);
    ?>
    </div>
    
</body>
</html>