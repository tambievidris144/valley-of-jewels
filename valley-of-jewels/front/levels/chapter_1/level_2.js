var game = new Phaser.Game(410,410, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create });

function preload(){
    game.load.image('diamond', '../../front/assets/images/little/particle.png');
    game.load.image('green', '../../front/assets/images/little/Green.png');
    game.load.image('blue', '../../front/assets/images/little/Blue.png');
    game.load.image('red', '../../front/assets/images/little/Red.png');
    game.load.image('purple', '../../front/assets/images/little/Purple.png');
    game.load.image('yellow', '../../front/assets/images/little/Yellow.png');
}
var points=0;
var emitter;
var arr=[];
var currentGemName;
var currentGem;
var N=70;
var group;
var columns=6;
var rows=6;
var gem;
var SelectedGemsCount=0;
var FirstGem;
var startX;
var startY;
var endX;
var endY;

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    emitter = game.add.emitter(0, 0, 100);
    group = game.add.group();
    CreateElement();
}

function CreateElement(){
    for(var i=0;i<columns;i++){
        for(var j=0;j<rows;j++){
            spawnGems(i,j);
        }
   }
            CheckOnDown();
            CheckOnRight();
            killGems();
}

function searchEmptyPositions(){
    for(var i=0;i<columns;i++){
        for(var j=0;j<rows;j++){
            currentGemName='gem' + i + 'x' + j;
            currentGem=group.iterate("name", currentGemName, Phaser.Group.RETURN_CHILD);
            if(currentGem==null){
                spawnGems(i,j);
            }
        }
    }
            CheckOnDown();
            CheckOnRight();
            killGems();
}

function spawnGems(i,j){
    var img=CreateElementImage();
    gem=group.create(i*N,-N,img);//координаты кристаллов
    gem.name = 'gem' + i.toString() + 'x' + j.toString();
    gem.color=img;
    gem.inputEnabled = true;
    gem.input.enableDrag();//следование за мышью
    gem.input.enableSnap(N, N , true, true);//перемещение вертикально или горизонтально по клику
    gem.posX=i;
    gem.posY=j;
    gem.events.onInputDown.add(SelectFirstGem, gem);
    gem.events.onInputUp.add(SelectSecondGem, gem);
    changePosition(gem,i*N,j*N);
}

function SelectFirstGem(gem){
    FirstGem=gem;
    startX=gem.x;
    startY=gem.y;
}

function SelectSecondGem(gem){
        endX=gem.position.x;
        endY=gem.position.y;
        var name='gem' + endX/N.toString() + 'x' + endY/N.toString();
        var gem2=group.iterate("name", name, Phaser.Group.RETURN_CHILD);
        //game.tweens.remove(gem2);
        swap(gem,gem2);
}

function swap(gem,gem2){
        if(checkMove()){
            changePosition(gem,endX,endY)
            changePosition(gem2,startX,startY)
            CheckOnDown();
            CheckOnRight();
            returnPositions(gem,gem2);
            killGems();
        }else{
            game.add.tween(gem).to( { x: startX, y: startY }, 500,Phaser.Easing.Linear.None, true);
        }
}

function returnPositions(gem,gem2){
    if(arr.length===0){
        changePosition(gem,startX,startY);
        changePosition(gem2,endX,endY);
    }
}


function checkMove(){
    if(startX==endX && endY==startY-1*N || startX==endX && endY==startY+1*N){
       return true;
   }else if(startY==endY && endX==startX-1*N || startY==endY && endX==startX+1*N){
       return true;
   }else {
        return false;
   }  
}

function killGems(){
    var deleteGemName;
    var deleteGemName;
    resultRender();
    if(arr.length>0){
        arr.forEach(function(item,i,arr){
            deleteGemName='gem' + item.x/N + 'x' + item.y/N;
            deleteGem=group.iterate("name", deleteGemName, Phaser.Group.RETURN_CHILD);
            particleEmiter(deleteGem.x,deleteGem.y);
            if(deleteGem.color=='red'){
                points=points+1;
            }
            deleteGem.kill();
            group.remove(deleteGem);
       })
      shiftDown();
      resultRender();
    }
}

function resultRender(){
    var string="";
    if(points>=25){
        var url = new URL(window.location.toString());
        var searchParams = new URLSearchParams(url.search.substring(1));
        var id = searchParams.get("id");
        $("#result").empty();
        string = `<span class="span-text">Вы выйграли</span> `;
        $('#result').html(string);

        string=`<span class="win">Победа</span>
        <div class="buttons-div">
        <form action="./new_level.php?id=${id}&level=3&ch=1&lvl=2&steps=0&time=0&res=Победа" method="post">
        <input type="submit" name="button" value="В меню" class="menu-button">
       </form>
        </div>`;
        $('#child-game_complete').html(string);
        $('#game_complete').attr('class', 'visible-game_complete');
    }else{
        string = `<span class="span-text red">${points}</span>`;
        $('#result').html(string);
    }
}

function particleEmiter(X,Y){
    emitter.makeParticles('diamond');
    emitter.gravity = 1000;
    emitter.x = X;
    emitter.y = Y;
    emitter.start(true, 1000, null, 4);
    game.time.events.add(1000, shiftDown);
    //shiftDown();
}


function CheckOnDown(){
    var tempArray=[];
    var count=0;
    var nextGemName;
    var nextGem;
    var tempGem;
    var nextJ=0;
    var tempGemName;
    console.log('CheckOnDown');
    for (var i=0; i<columns; i++){
        for(var j=0;j<rows;j++){
            tempGemName='gem' + i.toString() + 'x' + j.toString();
            tempGem=group.iterate("name", tempGemName, Phaser.Group.RETURN_CHILD);
            nextJ=j+1;
            if(nextJ<rows){
                nextGemName='gem' + i.toString() + 'x' + nextJ.toString();
                nextGem=group.iterate("name", nextGemName, Phaser.Group.RETURN_CHILD);
                if(tempGem.color==nextGem.color){
                    tempArray.push({x:tempGem.x,y:tempGem.y})
                    count++;
                }else if(tempGem.color!=nextGem.color && count==1){
                    tempArray.splice(tempArray.length-1,1);
                    count=0;
                }
                else if(tempGem.color!=nextGem.color && count>1){
                tempArray.push({x:tempGem.x,y:tempGem.y})
                count=0;
                }
            }else{
                if(count==1){
                    tempArray.splice(tempArray.length-1,1);
                    count=0;
                }else if(count>1){
                    tempArray.push({x:tempGem.x,y:tempGem.y})
                    count=0;
                }
            }
        }
    }

     for(var i=0;i<tempArray.length;i++){
                 arr.push({x:tempArray[i].x,y:tempArray[i].y});
         }
}

function CheckOnRight(){
    var tempArray=[];
    var count=0;
    var nextGemName;
    var nextGem;
    var tempGem;
    var nextJ=0;
    var tempGemName;
    console.log('CheckOnRight');
    for (var i=0; i<rows; i++){
        for(var j=0;j<columns;j++){
            tempGemName='gem' + j.toString() + 'x' + i.toString();
            tempGem=group.iterate("name", tempGemName, Phaser.Group.RETURN_CHILD);
            nextJ=j+1;
            if(nextJ<columns){
                nextGemName='gem' + nextJ.toString() + 'x' + i.toString();
                nextGem=group.iterate("name", nextGemName, Phaser.Group.RETURN_CHILD);

                if(tempGem.color==nextGem.color){
                    tempArray.push({x:tempGem.x,y:tempGem.y})
                    count++;
                }else if(tempGem.color!=nextGem.color && count==1){
                    tempArray.splice(tempArray.length-1,1);
                    count=0;
                }
                else if(tempGem.color!=nextGem.color && count>1){
                tempArray.push({x:tempGem.x,y:tempGem.y})
                count=0;
                }
            }else{
                if(count==1){
                    tempArray.splice(tempArray.length-1,1);
                    count=0;
                }else if(count>1){
                    tempArray.push({x:tempGem.x,y:tempGem.y})
                    count=0;
                }
            }
        }
    }
    
    for(var i=0;i<tempArray.length;i++){
        if(checkArray(tempArray[i].x,tempArray[i].y)==0){
                arr.push({x:tempArray[i].x,y:tempArray[i].y});
        }
    }
    
}

function checkArray(tempX,tempY){
    var elements=0;
    arr.forEach(function(item,i,arr){
        if(item.x==tempX && item.y==tempY){
            elements++;
        }
    })
    return elements;
}

function shiftDown(){
    var shiftGemName;
    var shiftGem;
    var currentY;
    console.log('SHiftDowm')
    for(var i=columns-1;i>=0;i--){
        for(var j=rows-1;j>0;j--){
            currentGemName='gem' + i + 'x' + j;
            currentGem=group.iterate("name", currentGemName, Phaser.Group.RETURN_CHILD);
            currentY=j;
            for(var z=j-1;z>=0;z--){
                shiftGemName='gem' + i + 'x' + z;
                shiftGem=group.iterate("name", shiftGemName, Phaser.Group.RETURN_CHILD);
                if(currentGem==null && shiftGem!=null){
                    changePosition(shiftGem,i*N,currentY*N);
                    currentGemName='gem' + i + 'x' + z;
                    currentGem=group.iterate("name", currentGemName, Phaser.Group.RETURN_CHILD);
                    currentY=z;
                }else if(currentGem==null && shiftGem==null){
                    continue;
                }else if(currentGem!=null){
                    currentGemName='gem' + i + 'x' + z;
                    currentGem=group.iterate("name", currentGemName, Phaser.Group.RETURN_CHILD);
                    currentY=z;
                    continue;
                }
            }
        }
    }
    arr=[];
    //game.time.events.add(1000, shiftDown);
    //game.time.events.add(1000, searchEmptyPositions);
    searchEmptyPositions();
}

function changePosition(changeGem,X,Y){
    game.add.tween(changeGem).to( { x: X, y: Y }, 600,Phaser.Easing.Linear.None, true);
    changeGem.name='gem' + X/N.toString() + 'x' + Y/N.toString();
    changeGem.x=X;
    changeGem.y=Y;
}

function CreateElementImage(i,j){
    var value = random(1, 5);
       switch(value){
           case 1:
               return 'green';
           break;
           case 2:
               return 'blue';
           break;
           case 3:
               return 'red';
           break;
           case 4:
               return 'purple';
           break;
           case 5:
               return 'yellow';
           break;
       }
}

function random(min, max) {
   var min = Math.ceil(min);
   var max = Math.floor(max);
   return Math.floor(Math.random() * (max - min + 1)) + min;
}

var string2=`<label for="" style="left: 10px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;Цель<br>
&nbsp;Уничтожить 25 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;красных
</label>
<div id="result">
</div>`;
$('.rules').html(string2);